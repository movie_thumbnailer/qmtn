# Packaging for Arch Linux

## Dependencies
Dependencies for building

```
pacman -S grep git binutils debugedit fakeroot gcc make awk file qt6-base
```

## Build and Install (-i)

```
cd pkgbuild
makepkg -i
```

