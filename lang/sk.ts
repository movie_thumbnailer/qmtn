<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk_SK" sourcelanguage="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../src/settingsdialog.ui" line="35"/>
        <location filename="../src/settingsdialog.ui" line="90"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="111"/>
        <source>mtn.e&amp;xe:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="157"/>
        <source>Insert New Profile</source>
        <translation>Vytvoriť nový profil</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="183"/>
        <source>Remove Current Profile</source>
        <translation>Odstrániť vybraný profil</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="205"/>
        <source>Profile:</source>
        <translation>Profil:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="218"/>
        <source>Timeout for executing mtn.exe (0 to disable timeout)</source>
        <translation>Čas na bez mtn.exe (0 neobmedzene)</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="221"/>
        <location filename="../src/settingsdialog.ui" line="372"/>
        <location filename="../src/settingsdialog.ui" line="470"/>
        <location filename="../src/settingsdialog.ui" line="532"/>
        <source>off</source>
        <translation>vyp.</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="227"/>
        <location filename="../src/settingsdialog.ui" line="421"/>
        <location filename="../src/settingsdialog.ui" line="600"/>
        <location filename="../src/settingsdialog.ui" line="616"/>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="240"/>
        <source>Timeout:</source>
        <translation>Max. čas:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="259"/>
        <source>Output</source>
        <translation>Výstup</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="299"/>
        <source>&amp;Columns:</source>
        <translation>&amp;Stĺpcov:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="309"/>
        <source>-c 3 : # of column</source>
        <translation>-c 3 : # stĺpcov</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="319"/>
        <source>&amp;Rows:</source>
        <translation>&amp;Riadkov:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="329"/>
        <source>-r 0 : # of rows; &gt;0:override -s</source>
        <translation>-r 0 : # riadkov; &gt;0 nahrádza -s</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="336"/>
        <source>&amp;Width:</source>
        <translation>&amp;Šírka:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="346"/>
        <source>-w 1024 : width of output image; 0:column * movie width</source>
        <translation>-w 1024 : šírka výstupného obrázka; 0: stĺpce * širka filmu</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="349"/>
        <location filename="../src/settingsdialog.ui" line="375"/>
        <location filename="../src/settingsdialog.ui" line="473"/>
        <location filename="../src/settingsdialog.ui" line="662"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="359"/>
        <source>&amp;Gap:</source>
        <translation>&amp;Medzera:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="369"/>
        <source>-g 0 : gap between each shot</source>
        <translation>-g 0 : medzera medzi každým obrázkom</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="385"/>
        <source>&amp;Suffix:</source>
        <translation>&amp;Prípona:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="395"/>
        <source>-o _s.jpg : output suffix</source>
        <translation>-o _s.jpg : prípojna v názve</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="398"/>
        <source>_s.jpg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="408"/>
        <source>S&amp;tep:</source>
        <translation>&amp;Krok:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="418"/>
        <source>-s 120 : time step between each shot</source>
        <translation>-s 120 : počet sekúnd medzi jednotlivými obrázkami</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="434"/>
        <source>Directory search recursion. 0: Selected videofiles only</source>
        <translation>Rekurzívne prehľadávanie priečinkov. 0: len vybrané súbory</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="447"/>
        <source>&amp;Depth:</source>
        <translation>&amp;Hĺbka:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="457"/>
        <source>S&amp;hadow:</source>
        <translation>&amp;Tiene:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="467"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;--shadow=N&lt;/p&gt;&lt;p&gt;draws shadows beneath thumbnails with radius N pixels if N&amp;gt;0;&lt;/p&gt;&lt;p&gt;Radius is computed if N=0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;--shadow=N&lt;/p&gt;&lt;p&gt;vykreslí tiene pod obrázkami so vzdialenosťou N pixelov pre N&amp;gt;0;&lt;/p&gt;&lt;p&gt;Vzdialenosť je vypočítaná ak N=0&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="486"/>
        <source>--cover: Extract album art</source>
        <translation>--cover: Ulož aj obal</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="489"/>
        <source>_cover.jpg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="499"/>
        <source>&amp;Cover:</source>
        <translation>&amp;Obal:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="519"/>
        <source>&amp;Edge Detect:</source>
        <translation>&amp;Detekcia hrán:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="529"/>
        <source>-D 12 : edge detection; 0:off &gt;0:on; higher detects more; try 4 6 or 8</source>
        <translation>-D 12 : detekcia hrán; 0:vypnutá &gt;0:zapnutá; Vyššie číslo detekuje viac; skús 4 6 alebo 8</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="548"/>
        <source>&amp;Blank skip:</source>
        <translation>&amp;Preskoč prázdne:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="558"/>
        <source>-b 0.80 : skip if % blank is higher; 0:skip all 1:skip really blank &gt;1:off</source>
        <translation>-b 0.80 : vynechaj ak je % vyššie; 0:vynechaj všetky 1:vynechaj veľmi prázdne &gt;1:vypnuté</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="574"/>
        <source>&amp;Quality:</source>
        <translation>&amp;Kvalita:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="584"/>
        <source>-j 90 : jpeg quality</source>
        <translation>-j 90 : kvalita jpeg</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="587"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="597"/>
        <source>-B 0.0 : omit this seconds from the beginning</source>
        <translation>-B 0.0 : počet sekúnd vynechaných na začiatku</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="613"/>
        <source>-E 0.0 : omit this seconds at the end</source>
        <translation>-E 0.0 : počet sekúnd vynechaných na konci</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="629"/>
        <source>Begin &amp;at:</source>
        <translation>&amp;Začiatok:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="639"/>
        <source>O&amp;mit from End:</source>
        <translation>&amp;Koniec:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="649"/>
        <source>&amp;Min. Height:</source>
        <translation>&amp;Min. výška:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="659"/>
        <source>-h 150 : minimum height of each shot; will reduce # of column to fit</source>
        <translation>-h 150 : minimálna výška každého obrázka; počet stĺpcov bude zredukovaný aby sa obrázok zmetil</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="678"/>
        <source>-v : verbose mode (debug)</source>
        <translation>-v : detailný režim (ladenie)</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="685"/>
        <source>&amp;Verbose:</source>
        <translation>&amp;Detailne:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="695"/>
        <source>Tra&amp;nsparent:</source>
        <translation>Prie&amp;hľadný:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="705"/>
        <source>--transparent: set background color (-k) to transparent; works with PNG image only</source>
        <translation>--transparent: nastavý priehľadnú farbu pozadia (-k); iba pre PNG</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="712"/>
        <source>&amp;Overwrite:</source>
        <translation>P&amp;repísať:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="722"/>
        <source>-W : dont overwrite existing files, i.e. update mode</source>
        <translation>-W : neprepisovať existujúce súbory</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="741"/>
        <source>Outp&amp;ut:</source>
        <translation>&amp;Výstup:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="751"/>
        <source>-O directory : save output files in the specified directory</source>
        <translation>-O priečinok : výstupný priečinok</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="757"/>
        <source>Use Source File Directory</source>
        <translation>Použiť pričinok vstupného súboru</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="779"/>
        <source>Select Output Directory</source>
        <translation>Vyber výstupný priečinok</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="782"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="818"/>
        <source>Style</source>
        <translation>Štýl</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="855"/>
        <source>Additional &amp;Title: </source>
        <translation>&amp;Voľiteľný popis: </translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="865"/>
        <source>-T text : add text above output image</source>
        <translation>-T text : pridá text nad obrázok</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="888"/>
        <source>Bac&amp;kground Color: </source>
        <translation>&amp;Farba pozadia: </translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="904"/>
        <source>-k RRGGBB : background color (in hex)</source>
        <translation>-k RRGGBB : barba pozadia (hexadecimálne)</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="940"/>
        <source>-i : info text on/off</source>
        <translation>-i : infotext zap/vyp</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="943"/>
        <source>&amp;Infotext</source>
        <translation>&amp;Informácie</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="981"/>
        <location filename="../src/settingsdialog.ui" line="1187"/>
        <source>Font:</source>
        <translation>Písmo:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1004"/>
        <source>Size:        </source>
        <translation>Veľkosť:        </translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1031"/>
        <location filename="../src/settingsdialog.ui" line="1234"/>
        <source>Location:</source>
        <translation>Pozícia:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1044"/>
        <location filename="../src/settingsdialog.ui" line="1247"/>
        <source>-L info_location[:time_location] : location of text</source>
        <translation>-L info pozícia[:pozícia času] : pozícia textu</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1057"/>
        <location filename="../src/settingsdialog.ui" line="1260"/>
        <source>lower left</source>
        <translation>vľavo dole</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1062"/>
        <location filename="../src/settingsdialog.ui" line="1265"/>
        <source>lower right</source>
        <translation>vpravo dole</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1067"/>
        <location filename="../src/settingsdialog.ui" line="1270"/>
        <source>upper right</source>
        <translation>vpravo hore</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1072"/>
        <location filename="../src/settingsdialog.ui" line="1275"/>
        <source>upper left</source>
        <translation>vľavo hore</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1087"/>
        <source>Color:</source>
        <translation>Farba:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1108"/>
        <source>Infotext Font Color</source>
        <translation>Farba textu informácií</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1149"/>
        <source>-t : time stamp on/off</source>
        <translation>-t : časová značka zap./vyp.</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1152"/>
        <source>Timestamp</source>
        <translation>Časové značky</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1210"/>
        <source>Font Size:</source>
        <translation>Veľkosť písma:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1280"/>
        <source>lower middle</source>
        <translation>v strede dole</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1285"/>
        <source>upper middle</source>
        <translation>v strede hore</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1301"/>
        <source>Color/Shadow/Box: </source>
        <translation>Farba/Tieň/Podklad: </translation>
    </message>
    <message>
        <source>Color/Shadow: </source>
        <translation type="vanished">Farba/tiene: </translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1311"/>
        <source>Timestamp Font Color</source>
        <translation>Farba písma časových značiek</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1330"/>
        <source>Timestamp Shadow Color</source>
        <translation>Farba tieňov časových značiek</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1360"/>
        <source>Timestamp Bounding Box Color</source>
        <translation>Farba podkladu časových značiek</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1401"/>
        <source>Additional options:</source>
        <translation>Ďalšie parametre:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1372"/>
        <source>Additional options not covered by form</source>
        <translation>Ďalšie parametre, ktoré nie sú pokryté vo formulári</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1420"/>
        <source>Export current settings to file</source>
        <translation>Exportovať aktuálne nastavenie do súbora</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1423"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1434"/>
        <source>Import settings from file</source>
        <translation>Import nastavenia zo súbora</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.ui" line="1437"/>
        <source>Import</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <location filename="../src/fileselector.cpp" line="38"/>
        <location filename="../src/fileselector.cpp" line="48"/>
        <source>Open File</source>
        <translation>Otvoriť súbor</translation>
    </message>
</context>
<context>
    <name>ImageItemView</name>
    <message>
        <location filename="../src/imageitemview.cpp" line="41"/>
        <source>Toggle &amp;Fullscreen</source>
        <translation>Prepnúť na celú &amp;obrazovku</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="46"/>
        <source>Zoom to Fit &amp;Window</source>
        <translation>Priblížiť podľa &amp;okna</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="50"/>
        <source>Zoom &amp;In (25%)</source>
        <translation>&amp;Priblížiť (25%)</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="55"/>
        <source>Zoom &amp;Out (25%)</source>
        <translation>&amp;Oddialiť (25%)</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="60"/>
        <source>&amp;Normal Size</source>
        <translation>&amp;Normálna veľkosť</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="64"/>
        <source>Open Image using &amp;Default Application</source>
        <translation>Otvoriť obrázok &amp;východzím programom</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="237"/>
        <source>Loading image failed!</source>
        <translation>Načítanie obrázka zlyhalo!</translation>
    </message>
    <message>
        <location filename="../src/imageitemview.cpp" line="243"/>
        <source>Preview not available</source>
        <translation>Náhľad nie je k dispozícii</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="17"/>
        <source>Movie Thumbnailer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="32"/>
        <source>Video Files</source>
        <translation>Video súbory</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="66"/>
        <source>Drag &amp; drop files here</source>
        <translation>Potiahni sem súbory</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="81"/>
        <source>Image</source>
        <translation>Obrázok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="130"/>
        <source>Log</source>
        <translation>Zápisník</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="176"/>
        <source>&amp;Edit</source>
        <translation>&amp;Úpravy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="182"/>
        <source>&amp;File</source>
        <translation>&amp;Súbor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="192"/>
        <location filename="../src/mainwindow.ui" line="251"/>
        <source>&amp;About</source>
        <translation>&amp;O Qmtn</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="203"/>
        <source>Toolbar</source>
        <translation>Nástroje</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="227"/>
        <source>&amp;Settings ...</source>
        <translation>&amp;Nastavenia ...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="230"/>
        <source>F10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="235"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončiť</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="238"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="243"/>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="246"/>
        <source>Ctrl+F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="254"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="259"/>
        <source>Open &amp;File ...</source>
        <translation>Otvoriť &amp;súbor ...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="262"/>
        <source>Open Video File</source>
        <translation>Otvoriť súbor videa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="265"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="270"/>
        <source>Open &amp;Directory ...</source>
        <translation>Otvoriť &amp;priečinok ...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="273"/>
        <source>Open Directory containing Video Files</source>
        <translation>Otvoriť priečinok obsahujúci video súbory</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="276"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="281"/>
        <location filename="../src/mainwindow.cpp" line="188"/>
        <location filename="../src/mainwindow.cpp" line="190"/>
        <source>&amp;Recreate Thumbnail</source>
        <translation>&amp;Obnoviť obrázok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="284"/>
        <source>Recreate Thumbnail</source>
        <translation>Obnoviť obrázok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="287"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="292"/>
        <source>Remove &amp;Item</source>
        <translation>Odobrať &amp;položku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="295"/>
        <source>Remove Item from Sidebar</source>
        <translation>Odobrať položku z panela</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="145"/>
        <source>Display labels</source>
        <translation>Zobratiť popisy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <source>Open &amp;Directory</source>
        <translation>Otvoriť &amp;priečinok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="180"/>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Open &amp;Movie</source>
        <translation>Spustiť &amp;film</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="185"/>
        <source>&amp;Expand all</source>
        <translation>&amp;Rozložiť všetky</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="186"/>
        <source>&amp;Collapse all</source>
        <translation>&amp;Zložiť všetky</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="533"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="514"/>
        <source>Overwrite</source>
        <translation>Prepísať</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="534"/>
        <source>Columns</source>
        <translation>Stĺpcov</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="536"/>
        <source>Step</source>
        <translation>Krok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>Rows</source>
        <translation>Riadkov</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="542"/>
        <source>Output</source>
        <translation>Výstup</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="545"/>
        <source>Suffix</source>
        <translation>Prípona</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="553"/>
        <source>Processing items</source>
        <translation>Spracúvam položky</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="568"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="573"/>
        <source>About...</source>
        <translation>O ...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="749"/>
        <source>Question</source>
        <translation>Otázka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="750"/>
        <source>Dou you want to upload file &apos;%1&apos; to &apos;%2&apos;?</source>
        <translation>Naozaj chcete nahrať súbor &apos;%1&apos; na &apos;%2&apos;?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="759"/>
        <location filename="../src/mainwindow.cpp" line="762"/>
        <source>Information</source>
        <translation>Informácia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="759"/>
        <source>Select a movie item</source>
        <translation>Vyberte video</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="762"/>
        <source>Nothing to upload</source>
        <translation>Nie je čo nahrať</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="91"/>
        <source>Cannon open file %1 for reading!</source>
        <translation>Nie je možné otvoriť %1 na čítanie!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="54"/>
        <source>Cannot create DataLocation %1!</source>
        <translation>Nie je možné vytvoriť DataLocation %1!</translation>
    </message>
    <message>
        <location filename="../src/profilemodel.cpp" line="63"/>
        <source>Cannot find DataLocation and thus store settings!</source>
        <translation>Nie je možné nájsť DataLocation a preto uložiť nastavenia!</translation>
    </message>
    <message>
        <location filename="../src/profilemodel.cpp" line="122"/>
        <source>Cannot open file %1!</source>
        <translation>Nie je možné otvoriť súbor %1!</translation>
    </message>
    <message>
        <location filename="../src/settingsdata.cpp" line="161"/>
        <source>Unknow error loading settings!</source>
        <translation>Neznáma chyba načítavania nastavení!</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="324"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="325"/>
        <source>Error loading settings!</source>
        <translation>Chyba načítania nastavení!</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/settingsdialog.cpp" line="167"/>
        <source>Output directory</source>
        <translation>Cieľový priečinok</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="183"/>
        <source>Select Color</source>
        <translation>Výber farby</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="252"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="248"/>
        <source>Name:</source>
        <translation>Názov:</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="268"/>
        <source>Delete profile</source>
        <translation>Zmazať profil</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="268"/>
        <source>Do you really want to remove &quot;%1&quot;?</source>
        <translation>Naozaj chcete zmazať &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="281"/>
        <source>Save File</source>
        <translation>Uložiť súbor</translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="283"/>
        <location filename="../src/settingsdialog.cpp" line="302"/>
        <source>JSON (*.json)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/settingsdialog.cpp" line="300"/>
        <source>Open File</source>
        <translation>Otvoriť súbor</translation>
    </message>
</context>
</TS>
